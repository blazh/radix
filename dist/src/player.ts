import { Observable } from "rxjs/Observable";
import * as child_process from "child_process";
import { Config } from "./config";

export class Player {

	config: Config;

	constructor() {
		this.config = new Config();
	}

	info = () => {
		return { "playing": this.config.getStreamingUrl(), "title": this.config.getTitle(), "volume": 100 };
	}

	play = (streamUrl: string, title?: string): Observable<any> => {
		return new Observable(observer => {
			child_process.exec('mpc clear', (err, stdout) => {
				if (err) {
					observer.error({ "error3": stdout, "streamUrl": streamUrl, "step": "clear" });
					observer.complete();
				}
				child_process.exec('mpc add "' + streamUrl + '"', (err, stdout) => {
					if (err) {
						observer.error({ "error4": stdout, "streamUrl": streamUrl, "step": "add" });
						observer.complete();
					}
					child_process.exec('mpc play', (err, stdout) => {
						if (err) {
							observer.error({ "error5": stdout, "streamUrl": streamUrl, "step": "play" });
							observer.complete();
						} else {
							this.config.setStreamingUrl(streamUrl);
							this.config.setTitle(title);
							observer.next(this.info());
							observer.complete();
						}
					});
				});
			});
		});
	}

}