"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Observable_1 = require("rxjs/Observable");
const child_process = require("child_process");
const config_1 = require("./config");
class Player {
    constructor() {
        this.info = () => {
            return { "playing": this.config.getStreamingUrl(), "title": this.config.getTitle(), "volume": 100 };
        };
        this.play = (streamUrl, title) => {
            return new Observable_1.Observable(observer => {
                child_process.exec('mpc clear', (err, stdout) => {
                    if (err) {
                        observer.error({ "error3": stdout, "streamUrl": streamUrl, "step": "clear" });
                        observer.complete();
                    }
                    child_process.exec('mpc add "' + streamUrl + '"', (err, stdout) => {
                        if (err) {
                            observer.error({ "error4": stdout, "streamUrl": streamUrl, "step": "add" });
                            observer.complete();
                        }
                        child_process.exec('mpc play', (err, stdout) => {
                            if (err) {
                                observer.error({ "error5": stdout, "streamUrl": streamUrl, "step": "play" });
                                observer.complete();
                            }
                            else {
                                this.config.setStreamingUrl(streamUrl);
                                this.config.setTitle(title);
                                observer.next(this.info());
                                observer.complete();
                            }
                        });
                    });
                });
            });
        };
        this.config = new config_1.Config();
    }
}
exports.Player = Player;
