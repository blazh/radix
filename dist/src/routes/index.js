/// <reference path="../_all.d.ts" />
"use strict";
const player_1 = require("../player");
const Observable_1 = require("rxjs/Observable");
var Route;
(function (Route) {
    class Index {
        constructor() {
            this.player = new player_1.Player();
            this.index = (req, res, next) => {
                res.send(JSON.stringify(this.player.info()));
            };
            this.youtube = (req, res, next) => {
                const v = req.query.id;
                const fs = require('fs');
                const ytdl = require('ytdl-core');
                const urlPipe = new Observable_1.Observable(observer => {
                    ytdl('http://www.youtube.com/watch?v=' + v, { filter: (format) => {
                            // console.log(format.url)
                            observer.next(format.url);
                            observer.complete();
                            return format === format;
                        } });
                });
                urlPipe.subscribe(url => {
                    console.log(url.toString());
                    this.player.play(url.toString()).subscribe(r => {
                        res.send(JSON.stringify(this.player.info()));
                    }, err => {
                        res.send(JSON.stringify(err));
                    });
                    // res.send(JSON.stringify(url));
                });
                //  .pipe(fs.createWriteStream('video.mp3'));
            };
            this.play = (req, res, next) => {
                var station = req.query.url;
                var title = req.query.title;
                this.player.play(station, title).subscribe(res => {
                    res.send(JSON.stringify(this.player.info()));
                }, err => {
                    res.send(JSON.stringify(err));
                });
            };
        }
    }
    Route.Index = Index;
})(Route || (Route = {}));
module.exports = Route;
