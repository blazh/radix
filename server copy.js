const express = require('express');
const app = express();
const path = require('path');
const querystring = require('querystring');
const http = require('http');
const WebSocket = require('ws');
const bodyParser     = require('body-parser');
const PlayMusic = require('playmusic');
const server = http.createServer(app);
const wss = new WebSocket.Server({ server });
const port = 8090;

var fs = require('fs'),
nconf = require('nconf');
var pm = new PlayMusic();
var currentStation;
var currentStationTitle;
var library = [];
var status = 0;
const { URL } = require('url');

var self = this;


app.get('/', function(req, res) {
    child_process.exec("awk -F'[][]' '/dB/ { print $2 }' <(amixer sget Speaker | grep 'Left:')", function(err, out, code) {
        res.send(JSON.stringify({ "playing": currentStation, "title": currentStationTitle, "volume": 100 }));
    });
});

app.get('/play', function(req, res) {
    currentStation = req.query.url;
    currentStationTitle = req.query.title;
    child_process.exec('mpc clear');
    child_process.exec('mpc add ' + currentStation, function(err, out, code) {
        child_process.exec('mpc play', function(err, out, code) {
            nconf.set('play', currentStation);
            nconf.set('title', currentStationTitle);
            nconf.save(function (err) {
                fs.readFile('/root/radix/config.json', function (err, data) {});
            });
        });
    });
    res.send(JSON.stringify({ "result": "Playing " + currentStation }));
});

app.get('/next', function(req, res) {
    child_process.exec('mpc next', function(err, out, code) {
        if (err) {
            res.send(JSON.stringify({ "error": JSON.stringify(err) }));
            return;
        }
        res.send(JSON.stringify({ "result": "Skipped" }));
    });
});

app.get('/repeat', function(req, res) {
    child_process.exec('mpc repeat', function(err, out, code) {
        if (err) {
            res.send(JSON.stringify({ "error": JSON.stringify(err) }));
            return;
        }
        res.send(JSON.stringify({ "result": JSON.stringify(out) }));
    });
});

app.get('/volume', function(req, res) {
    var sign = "+";
    if (req.query.sign == "minus") sign = "-";
    child_process.exec("amixer sset 'Speaker' 2" + sign, function(err, out, code) {

    });
    res.send(JSON.stringify({ "result": "Volume set to " + req.query.set }));
});

app.get('/vol', function(req, res) {
    child_process.exec("amixer sset 'Speaker' " + req.query.set + "%", function(err, out, code) {
        res.send(JSON.stringify({ "result": "Volume set to " + req.query.set }));
    });
});

wss.on('connection', function connection(ws, req) {
    ws.send(JSON.stringify({ "result": "started" }));
});

server.listen(port, () => {
    nconf.argv()
    .env()
    .file({ file: '/root/radix/config.json' });
    var url = nconf.get('play');

    // force default radio stream if GoogleMusicPlay was last used; url has expirantion that therefore is not able to be sed again
    if (url && url.indexOf("googleusercontent") > -1) {
        url = null;
    }

    if (!url) {
        url = "http://178.32.62.172:8878/;";
    } else {
        url = decodeURI(url);
        currentStation = url;
        currentStationTitle = nconf.get('title');
    }
    child_process.exec('mpc add ' + url, function(err, out, code) {
        child_process.exec('mpc play', function(err, out, code) {
        });
    });
    console.log('We are live on ' + port);
    status = 100;
    // initialze google library
    var username = nconf.get('google_username');
    var password = nconf.get('google_password');
    username = "faciron@gmail.com";
    password = "sev82bsv";
    if (username && password) {
        pm.init({email: username, password: password}, function(err) {
            if (err) console.error(err);
            console.log("Initialized GoogleMusicPlay");
            refreshLibrary();
        })
    }
});
