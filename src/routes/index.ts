/// <reference path="../_all.d.ts" />
"use strict";

import * as express from "express";
import { Player } from "../player";

import * as YouTubePlayer from 'youtube-player';
import { Observable } from "rxjs/Observable";

module Route {

  export class Index {

    player: Player = new Player();

    constructor() {}

    public index = (req: express.Request, res: express.Response, next: express.NextFunction): void => {
      res.send(JSON.stringify(this.player.info()));
    }

    public youtube = (req: express.Request, res: express.Response, next: express.NextFunction): void => {
      const v = req.query.id;
      const fs = require('fs');
      const ytdl = require('ytdl-core');
      const urlPipe = new Observable(observer => {
        ytdl('http://www.youtube.com/watch?v=' + v, { filter: (format) => {
          // console.log(format.url)
          observer.next(format.url);
          observer.complete();
          return format === format
        }})
      })

      urlPipe.subscribe(url => {
        console.log(url.toString())

        this.player.play(url.toString()).subscribe(r => {
          res.send(JSON.stringify(this.player.info()));
        }, err => {
          res.send(JSON.stringify(err));
        });


        // res.send(JSON.stringify(url));
      })
      //  .pipe(fs.createWriteStream('video.mp3'));
    }

    public play = (req: express.Request, res: express.Response, next: express.NextFunction): void => {
      var station = req.query.url;
      var title = req.query.title;
      this.player.play(station, title).subscribe(res => {
        res.send(JSON.stringify(this.player.info()));
      }, err => {
        res.send(JSON.stringify(err));
      });
    }
    
  }
}

export = Route;