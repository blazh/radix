"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nconf = require("nconf");
const fs = require("fs");
class Config {
    constructor() {
        nconf.argv()
            .env()
            .file({ file: 'config.json' });
    }
    getStreamingUrl() {
        return nconf.get('url');
    }
    setStreamingUrl(url) {
        nconf.set('url', url);
        this.save();
    }
    getTitle() {
        return nconf.get('title');
    }
    setTitle(title) {
        nconf.set('title', title);
        this.save();
    }
    save() {
        nconf.save(function (err) {
            fs.readFile('config.json', function (err, data) { });
        });
    }
}
exports.Config = Config;
