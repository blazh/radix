import * as nconf from 'nconf';
import * as fs from 'fs';

export class Config {

    constructor() {
        nconf.argv()
        .env()
        .file({ file: 'config.json' });
    }

    getStreamingUrl(): string {
        return nconf.get('url');
    }

    setStreamingUrl(url: string): void {
        nconf.set('url', url);
        this.save();
    }

    getTitle(): string {
        return nconf.get('title');
    }

    setTitle(title: string): void {
        nconf.set('title', title);
        this.save();
    }

    private save() {
        nconf.save(function (err) {
            fs.readFile('config.json', function (err, data) {});
        });
    }

}